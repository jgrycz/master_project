# -*- coding: utf-8 -*-
"""
:author: Jaroslaw Grycz
:contact: jaroslaw.grycz@gmail.com
"""
from setuptools import setup


setup(name='master_project',
      version='0.1',
      description=('Multistage computer-aided of anemiia in children '
                   '- algorithms and experimental investigations.'),
      author='Jaroslaw Grycz',
      author_email='jaroslaw.grycz@gmail.com')
