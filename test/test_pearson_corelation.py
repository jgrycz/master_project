# -*- coding: utf-8 -*-
"""
:author: Jaroslaw Grycz
:contact: jaroslaw.grycz@gmail.com
"""
from master_project.pearson_corelation import Pearsonr, Corelation, CorelationContainer
from master_project.classes import Anemia
from master_project.range import Range
from unittest import TestCase
import numpy as np


class TestPearsonr(TestCase):
    def setUp(self):
        self.data = np.array([[1, 1, 1], [2, 2, 2], [3, 3, 3]])

    def test_calculate_for_column(self):
        anemia = Anemia('t2', Range(2, 3))
        dataset = anemia.prepare_data(self.data)
        p = Pearsonr('name')
        np.testing.assert_array_equal(p.calculate_for_column(dataset, 1), [0.0])

    def test_calculate_for_all_columns(self):
        anemia = Anemia('t2', Range(2, 3))
        dataset = anemia.prepare_data(self.data)
        p = Pearsonr('name')
        corelation_container = p.calculate_for_all_columns(dataset)
        assert corelation_container.get_attributes() == [1, 2]
        assert corelation_container.get_corelations() == [0, 0]

    def test_make_permutation(self):
        data = np.array([[1, 2, 3, 4], [1, 2, 3, 4], [1, 2, 3, 4]])
        p = Pearsonr('name')
        p.container = CorelationContainer('test')
        p.container.add_data(Corelation(1, 0))
        p.container.add_data(Corelation(2, 0.5))
        p.container.add_data(Corelation(3, 1))
        expected_matrix = [[1, 4, 3, 2], [1, 4, 3, 2], [1, 4, 3, 2]]
        np.testing.assert_array_equal(p.make_permutation(data), expected_matrix)


class TestCorelation(TestCase):
    def test_create_obj(self):
        cor = Corelation(1, -1)
        assert (1, 1) == (cor.attribute_number, cor.corelation)

        cor = Corelation(1, 1)
        assert (1, 1) == (cor.attribute_number, cor.corelation)


class TestCorelationContainer(TestCase):
    def setUp(self):
        self.container = CorelationContainer('test')
        self.cor1 = Corelation(3, 2)
        self.cor2 = Corelation(1, 4)
        self.container.add_data(self.cor1)
        self.container.add_data(self.cor2)

    def test_add_data(self):
        assert len(self.container.container) == 2
        assert self.container.container[0] is self.cor1
        assert self.container.container[1] is self.cor2

    def test_get_attributes(self):
        assert self.container.get_attributes() == [3, 1]

    def test_get_corelations(self):
        assert self.container.get_corelations() == [2, 4]

    def test_get_sorted_data(self):
        assert self.container.get_sorted_data() == [self.cor2, self.cor1]

        assert self.container.get_sorted_data(sorted_by='attribute_number') == \
            [self.cor1, self.cor2]
