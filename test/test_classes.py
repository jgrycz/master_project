# -*- coding: utf-8 -*-
"""
:author: Jaroslaw Grycz
:contact: jaroslaw.grycz@gmail.com
"""
from master_project.classes import Anemia
from master_project.range import Range
from unittest import TestCase
import numpy as np


class TestAnemia(TestCase):
    def test_iter_on_all_nodes_one_node(self):
        anemias = Anemia('t1', Range(1, 2), [Anemia('t1', Range(1)), Anemia('t1', Range(2))])
        assert len(list(anemias.iter_on_all_nodes())) == 1

    def test_iter_on_all_nodes_complex_tree(self):
        n1 = Anemia('t1', Range(1, 2), [Anemia('t1', Range(1)), Anemia('t1', Range(2))])
        n2 = Anemia('t2', Range(3, 4), [Anemia('t1', Range(3)), Anemia('t1', Range(4))])
        root = Anemia('root', Range(1, 4), [n1, n2])
        assert len(list(root.iter_on_all_nodes())) == 3

    def test_prepare_target_classes(self):
        data = np.array([[1, 1, 1], [2, 2, 2], [3, 3, 3]])
        anemia = Anemia('t2', Range(2))
        np.testing.assert_array_equal(anemia.prepare_target_classes(data, 0), [1])

        anemia = Anemia('t0', Range(1, 2), [Anemia('t1', Range(1)),
                                            Anemia('t2', Range(2))])
        np.testing.assert_array_equal(anemia.prepare_target_classes(data, 0), [1, 2])
