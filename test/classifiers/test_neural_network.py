# -*- coding: utf-8 -*-
"""
:author: Jaroslaw Grycz
:contact: jaroslaw.grycz@gmail.com
"""
from master_project.classifiers import NeuralNetwork
from unittest import TestCase
import numpy as np


class TestNeuralNetwork(TestCase):
    def test_prepare_datasets(self):
        dataset = np.arange(1, 26, 1).reshape(5, 5)
        neural_network = NeuralNetwork(dataset)
        training, testing = neural_network.prepare_datasets()
        for row in training:
            assert row not in testing
        assert training.shape == (2, 5)
        assert testing.shape == (3, 5)

    def test_make_test(self):
        xor = np.array([[0, 0, 0], [1, 0, 1], [1, 1, 0], [0, 1, 1]])
        neural_network = NeuralNetwork(xor)
        neural_network.train(xor[:, 1:], xor[:, 0])
        assert (4, 0) == neural_network.test(xor[:, 1:], xor[:, 0])
