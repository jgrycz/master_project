# -*- coding: utf-8 -*-
"""
:author: Jaroslaw Grycz
:contact: jaroslaw.grycz@gmail.com
"""
from master_project.classifiers import NearestCentroid
from unittest import TestCase
import numpy as np


class TestNearestCentroid(TestCase):
    def test_prepare_datasets(self):
        dataset = np.arange(1, 26, 1).reshape(5, 5)
        nearest_centroid = NearestCentroid(dataset)
        training, testing = nearest_centroid.prepare_datasets()
        for row in training:
            assert row not in testing
        assert training.shape == (2, 5)
        assert testing.shape == (3, 5)

    def test_make_test(self):
        xor = np.array([[0, 0, 0], [1, 0, 1], [1, 1, 0], [0, 1, 1]])
        nearest_centroid = NearestCentroid(xor)
        nearest_centroid.train(xor[:, 1:], xor[:, 0])
        assert (2, 2) == nearest_centroid.test(xor[:, 1:], xor[:, 0])
