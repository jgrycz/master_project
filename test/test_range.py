# -*- coding: utf-8 -*-
"""
:author: Jaroslaw Grycz
:contact: jaroslaw.grycz@gmail.com
"""
from master_project.range import Range, WrongNuberOfElementsInRange, GreaterFirstValue
from unittest import TestCase
import numpy as np


class TestRange(TestCase):
    def test_fail_create_range(self):
        with self.assertRaises(WrongNuberOfElementsInRange):
            Range(1, 2, 3)

        with self.assertRaises(WrongNuberOfElementsInRange):
            Range()

        with self.assertRaises(GreaterFirstValue):
            Range(3, 1)

    def test_single_element_range(self):
        r1 = Range(2)
        r2 = Range(2)
        assert r2 in r1
        assert r2 == r1

        assert 2 in r1
        assert 3 not in r1
        assert 2 == r1
        assert 3 != r1

        r3 = Range(3)
        assert r3 not in r1
        assert r3 != r1

    def test_two_elements_range(self):
        r1 = Range(2, 4)
        assert 2 in r1
        assert 4 in r1
        assert 5 not in r1

        assert 2 != r1
        assert 3 != r1

        r2 = Range(2)
        assert r2 in r1
        assert r2 != r1

        r4 = Range(4)
        assert r4 in r1
        assert r4 != r1

        r5 = Range(5)
        assert r5 not in r1
        assert r5 != r1

    def test_single_value_range_prepare_data(self):
        data = np.array([[1, 1, 1], [2, 2, 2], [3, 3, 3]])
        r = Range(2)
        np.testing.assert_array_equal(r.get_mask(data), [False, True, False])

    def test_bounded_range_prepare_data(self):
        data = np.array([[1, 1, 1], [2, 2, 2], [3, 3, 3]])
        r = Range(2, 3)
        np.testing.assert_array_equal(r.get_mask(data), [False, True, True])

    def test_single_value_range_get_data(self):
        data = np.array([[1, 1, 1], [2, 2, 2], [3, 3, 3]])
        r = Range(2)
        np.testing.assert_array_equal(r.get_data(data), [[2, 2, 2]])

    def test_bounded_range_get_data(self):
        data = np.array([[1, 1, 1], [2, 2, 2], [3, 3, 3]])
        r = Range(2, 3)
        np.testing.assert_array_equal(r.get_data(data), [[2, 2, 2], [3, 3, 3]])
