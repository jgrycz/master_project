# -*- coding: utf-8 -*-
"""
:author: Jaroslaw Grycz
:contact: jaroslaw.grycz@gmail.com
"""
from scipy.stats import pearsonr
from operator import attrgetter
from pprint import pprint

import numpy as np


class Pearsonr:
    def __init__(self, name, class_index=0):
        self.name = name
        self.class_index = class_index
        self.container = None

    def calculate_for_all_columns(self, dataset):
        columns = dataset.shape[1]
        self.container = CorelationContainer(self.name)
        for column in range(1, columns):
            coefficient = self.calculate_for_column(dataset, column)
            self.container.add_data(Corelation(column, coefficient))
        return self.container

    def calculate_for_column(self, dataset, column):
        coefficient, _ = pearsonr(dataset[:, column], dataset[:, self.class_index])
        if np.isnan(coefficient):
            coefficient = 0.0
        return coefficient

    def make_permutation(self, dataset, treshold=0.0, row=None):
        if self.container is None:
            self.calculate_for_all_columns(dataset)
        columns_order = [c.attribute_number for c in self.container.get_sorted_data()]
        columns_order.insert(0, 0)
        columns_included = len([c for c in self.container.get_sorted_data()
                                if c.corelation >= treshold]) + 1
        perumtation = np.argsort(columns_order)
        if row is None:
            return dataset[:, perumtation][:, :columns_included]
        else:
            return row[perumtation]


class Corelation:
    def __init__(self, attribute_number, corelation):
        self.attribute_number = attribute_number
        self.corelation = abs(corelation)

    def __str__(self):
        return "({:2}, {})".format(self.attribute_number, self.corelation)

    __repr__ = __str__


class CorelationContainer:
    def __init__(self, name):
        self.name = name
        self.container = []

    def add_data(self, corelation_obj):
        self.container.append(corelation_obj)

    def get_attributes(self):
        return [el.attribute_number for el in self.container]

    def get_corelations(self):
        return [el.corelation for el in self.container]

    def get_data(self):
        return self.container

    def get_sorted_data(self, sorted_by='corelation'):
        if sorted_by not in ('corelation', 'attribute_number'):
            raise WrongOrderParam('Can not print data sorted by {}'.format(sorted_by))
        sorted_by = attrgetter(sorted_by)
        return sorted(self.container, key=lambda x: sorted_by(x), reverse=True)

    def print_data(self, sorted_by='corelation'):
        data_to_print = self.get_sorted_data(sorted_by)
        print("Calculate corelation for: {}\n".format(self.name))
        pprint(data_to_print)
        print("\n{}\n\n".format("=" * 60))

    def plot(self, plot=True, subplot=None, attrib_order=True):
        import matplotlib.pyplot as plt
        if subplot is not None:
            plt.subplot(*subplot)

        if attrib_order:
            plt.plot(self.get_attributes(), self.get_corelations(), "ro")
            plt.xticks(np.arange(1, self.container[-1].attribute_number + 1, 1.0))
        else:
            plt.plot(self.get_attributes(), sorted(self.get_corelations(), reverse=True), "ro")
            plt.xticks(self.get_attributes(), [e.attribute_number for e in self.get_sorted_data()])

        # plt.title(self.name)
        plt.xlim(0, self.container[-1].attribute_number + 1, 1)
        ax = plt.gca()
        ax.set_xlabel('Cechy', fontsize=9)
        # ax.xaxis.set_label_coords(0.95, -0.15)
        ax.set_ylabel('Współczynnik korelacji', fontsize=9)
        # ax.yaxis.set_label_coords(-0.03, 0.7)

        if plot:
            mng = plt.get_current_fig_manager()
            mng.resize(*mng.window.maxsize())
            plt.subplots_adjust(left=0.05, bottom=0.05, right=0.99,
                                top=0.96, wspace=0.13, hspace=0.50)
            plt.show()


class WrongOrderParam(Exception):
    pass
