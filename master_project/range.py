# -*- coding: utf-8 -*-
"""
:author: Jaroslaw Grycz
:contact: jaroslaw.grycz@gmail.com
"""
from abc import ABCMeta, abstractmethod


class BaseRange(metaclass=ABCMeta):

    def __eq__(self, other):
        if issubclass(type(other), BaseRange):
            return self.start == other.start and self.stop == other.stop
        else:
            return self.start == other == self.stop

    def __ne__(self, other):
        return not other == self

    @abstractmethod
    def get_mask(self, array):
        pass

    def get_data(self, array):
        return array[self.get_mask(array)]


class BoundedRange(BaseRange):

    def __init__(self, *args):
        self.start, self.stop = args
        super().__init__()

    def __str__(self):
        return "({}, {})".format(self.start, self.stop)

    def __repr__(self):
        return "Range({}, {})".format(self.start, self.stop)

    def __iter__(self):
        yield from range(self.start, self.stop + 1)

    def get(self):
        return (self.start, self.stop)

    def get_mask(self, array, index=0):
        return (self.start <= array[:, index]) & (array[:, index] <= self.stop)


class SingleValueRange(BaseRange):
    def __init__(self, *args):
        self.start, = self.stop, = args
        super().__init__()

    def __str__(self):
        return str(self.start)

    def __repr__(self):
        return "Range({})".format(self.start, self.stop)

    def __iter__(self):
        yield self.start

    def get(self):
        return self.start

    def get_mask(self, array, index=0):
        return array[:, index] == self.start


class Range:
    def __new__(cls, *args, **kwargs):
        length = len(args)
        if length not in (1, 2):
            raise WrongNuberOfElementsInRange('Range object could not handle {!r}'.format(args))
        if length == 1:
            return SingleValueRange(*args)
        if args[0] > args[1]:
            raise GreaterFirstValue('First value clould not to be greater than second.')
        if length == 2:
            return BoundedRange(*args)


class WrongNuberOfElementsInRange(Exception):
    pass


class GreaterFirstValue(Exception):
    pass
