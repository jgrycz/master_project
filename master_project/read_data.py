# -*- coding: utf-8 -*-
"""
:author: Jaroslaw Grycz
:contact: jaroslaw.grycz@gmail.com
"""
from pkg_resources import resource_filename
from numpy import genfromtxt


DATA_FILE_PATH = resource_filename('master_project', "data/ANEMIA.csv")


def normalize_data_file():
    with open(DATA_FILE_PATH, 'r') as f:
        lines = f.readlines()

    cls = None
    new_content = []
    for line in lines:
        line = line.replace('\n', '')
        clasificators = list(filter(lambda line: line != '', line.split(',')))
        if len(clasificators) == 33:
            cls = clasificators[0]
        else:
            clasificators.insert(0, cls)
        new_content.append(clasificators)

    with open(DATA_FILE_PATH, 'w') as f:
        f.write("\n".join(",".join(line) for line in new_content))


def get_dataset():
    return genfromtxt(DATA_FILE_PATH, delimiter=',')


def get_normalized_dataset():
    dataset = get_dataset()
    features = get_dataset()[:, 1:]
    normalized_features = features / features.max(axis=0)
    dataset[:, 1:] = normalized_features
    return dataset
