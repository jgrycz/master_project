# -*- coding: utf-8 -*-
"""
:author: Jaroslaw Grycz
:contact: jaroslaw.grycz@gmail.com
"""
from master_project.range import Range, BoundedRange
from treelib import Tree
import numpy as np


class Anemia:
    def __init__(self, type, range, subtypes=None):
        self.type = type
        self.range = range
        self.subtypes = subtypes

    def __repr__(self):
        return "Anemia(type={!r}, range={}, subtypes={})".format(self.type,
                                                                 repr(self.range),
                                                                 self.subtypes)

    def __str__(self):
        return "{}. {}".format(self.range, self.type)

    def __iter__(self):
        if self.subtypes:
            yield from self.subtypes

    def iter_on_all_nodes(self):
        if self.subtypes:
            yield self
            for subtype in self.subtypes:
                yield from subtype.iter_on_all_nodes()

    def __len__(self):
        return len(self.subtypes)

    def print_as_tree(self, tree=None):
        show = True if tree is None else False
        if tree is None:
            tree = Tree()
            tree.create_node(str(self), self.range.get())    # root node

        for subtype in self:
            tree.create_node(str(subtype), subtype.range.get(), self.range.get())
            subtype.print_as_tree(tree)

        if show:
            tree.show(key=lambda n: n.identifier)

    def prepare_data(self, base_dataset, class_index=0):
        target_data = self.range.get_data(base_dataset)
        target_classes = self.prepare_target_classes(base_dataset, class_index)
        target_data[:, 0] = target_classes
        return target_data

    def prepare_target_classes(self, base_dataset, class_index):
        target = None
        if self.subtypes is None:
            target = self.range.get_mask(base_dataset, index=class_index)
            target = target.astype(np.int)
        else:
            rows, _ = base_dataset.shape
            target = np.zeros(rows, np.int)
            for num, subtype in enumerate(self, 1):
                mask = subtype.range.get_mask(base_dataset, index=class_index)
                target += mask * num
        return np.trim_zeros(target)

    def is_node(self):
        return type(self.range) is BoundedRange


all_classes = [Anemia('normocytowa', Range(1)),
               Anemia('megaloblastyczna', Range(2)),
               Anemia('z niedoboru żelaza', Range(3)),
               Anemia('pierwotnie aplastyczna', Range(4)),
               Anemia('wtórnie aplastyczna', Range(5)),
               Anemia('wrodzona sferocytoza', Range(6)),
               Anemia('wrodzona eliptocytoza', Range(7)),
               Anemia('wrodzona stomatocytoza', Range(8)),
               Anemia('wrodzona akantocytoza', Range(9)),
               Anemia('fawizm - choroba konskiej fasoli', Range(10)),
               Anemia('kinaza kwasu pirogronowego', Range(11)),
               Anemia('srodziemnomorska (talesmia)', Range(12)),
               Anemia('sierpowatość', Range(13)),
               Anemia('z autoimmuno przeciwciałami', Range(14)),
               Anemia('z alloimunoprzeciwciałami', Range(15)),
               Anemia('polekowa', Range(16)),
               Anemia('z powodu zraniena', Range(17)),
               Anemia('z powodu pasozytów', Range(18)),
               Anemia('w przebiegu polipowatości jelita', Range(19)),
               Anemia('z powodu wrzodu', Range(20))]


erytropeniczna = [Anemia('5. Niedokrwistość tropiczna',
                         Range(1, 3), all_classes[0:3]),
                  Anemia('6. Niedokrwistość aplastyczna',
                         Range(4, 5), all_classes[3:5])]

hemolityczna = [Anemia('7. Spowodowana defektem blony komorkowej',
                       Range(6, 9), all_classes[5:9]),
                Anemia('8. Spowodowana niedoborem enzymow erytrocyta',
                       Range(10, 11), all_classes[9:11]),
                Anemia('9. Spowodowana nieprawidlowościa hemoglobiny',
                       Range(12, 13), all_classes[11:13]),
                Anemia('10. Spowodowana immunologicznie',
                       Range(14, 16), all_classes[13:16])]

anemia = Anemia('1. Anemia u dzieci', Range(1, 20),
                [Anemia('2. Niedokrwistość erytropeniczna',
                        Range(1, 5), erytropeniczna),
                 Anemia('3. Niedokrwistość hemolityczna',
                        Range(6, 16), hemolityczna),
                 Anemia('4. Niedokrwistość pokrwotoczna',
                        Range(17, 20), all_classes[16:])])
