# -*- coding: utf-8 -*-
"""
:author: Jaroslaw Grycz
:contact: jaroslaw.grycz@gmail.com
"""
from pkg_resources import resource_filename
from pprint import pprint as pp
import numpy as np
import json
import os


class Ploter:
    ROOT = os.path.join(os.path.split(os.path.split(os.path.abspath(__file__))[0])[0],
                        'docs', 'images')

    def __init__(self):
        self.results = self.read_results()

    def normalize_name(self, name):
        return name.split('). ')[1].\
            replace('ą', 'a').\
            replace('ć', 'c').\
            replace('ę', 'e').\
            replace('ł', 'l').\
            replace('ń', 'n').\
            replace('ó', 'o').\
            replace('ś', 's').\
            replace('ź', 'z').\
            replace('ż', 'z').\
            replace(' ', '_').\
            replace('.', '')

    def read_results(self):
        with open(self.RESULTS_PATH, 'r') as f:
            results = json.load(f)
        return results

    def maximalize_window(self, plt):
        mng = plt.get_current_fig_manager()
        mng.resize(*mng.window.maxsize())
        plt.subplots_adjust(left=0.05, bottom=0.1, right=0.99, top=0.99, wspace=0.13, hspace=0.50)

    def set_labels(self, plt, xl='Ilość cech', yl='% skutecznosci', fontsize=9):
        ax = plt.gca()
        ax.set_xlabel(xl, fontsize=fontsize)
        ax.set_ylabel(yl, fontsize=fontsize)

    def set_legend(self, plt, lines, colors):
        ax = plt.gca()
        ax.legend(lines, colors)

    def set_size(self, fig, h, w):
        fig.set_figheight(h)
        fig.set_figwidth(w)


class BestResults(Ploter):
    RESULTS_PATH = resource_filename('master_project', "results/best_results.json")
    IMG_DIR_NAME = 'best'

    def __init__(self):
        super().__init__()

    def plot_times(self, show=True, save=False):
        import matplotlib.pyplot as plt

        centroid = [result[0]['ratio'] for result in self.results]
        neighbors = [result[1]['ratio'] for result in self.results]
        network = [result[2]['ratio'] for result in self.results]
        labels = [result[0]['id'] for result in self.results]
        ind = np.arange(len(centroid))  # the x locations for the groups
        width = 0.25       # the width of the bars

        fig, ax = plt.subplots()
        centroid_bar = ax.bar(ind, centroid, width, color='r')
        neighbors_bar = ax.bar(ind + width, neighbors, width, color='y')
        network_bar = ax.bar(ind + 2 * width, network, width, color='g')

        # # add some text for labels, title and axes ticks
        ax.set_ylabel('Procent trafień')
        ax.set_xlabel('Numer węzła')
        ax.set_title('Porównanie najlepszych wyników dla poszczególnych węzłów')
        ax.set_xticks(ind + width)
        ax.set_yticks(np.arange(0, 110, 10))
        ax.set_xticklabels(labels)
        ax.legend((centroid_bar[0], neighbors_bar[0], network_bar[0]),
                  ('Najbliższa średnia', 'K-najbliższych sąsiadów', 'Sieć neuronowa'))
        self.maximalize_window(plt)

        if save:
            extent = fig.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
            img_path = os.path.join(self.ROOT, self.IMG_DIR_NAME, 'best_results.png')
            fig.savefig(img_path, bbox_inches=extent.expanded(1.2, 1.2))

        if show:
            self.maximalize_window(plt)
            plt.show()

    def plot_number_of_features(self, show=True, save=False):
        import matplotlib.pyplot as plt

        centroid = [result[0]['features'] for result in self.results]
        neighbors = [result[1]['features'] for result in self.results]
        network = [result[2]['features'] for result in self.results]
        labels = [result[0]['id'] for result in self.results]
        ind = np.arange(len(centroid))  # the x locations for the groups
        width = 0.25       # the width of the bars

        fig, ax = plt.subplots()
        centroid_bar = ax.bar(ind, centroid, width, color='r')
        neighbors_bar = ax.bar(ind + width, neighbors, width, color='y')
        network_bar = ax.bar(ind + 2 * width, network, width, color='g')

        # # add some text for labels, title and axes ticks
        ax.set_ylabel('Ilość użytych cech')
        ax.set_xlabel('Numer węzła')
        ax.set_title('Porównanie ilości cech dla najlepszych wyników dla poszczególnych węzłów')
        ax.set_xticks(ind + width)
        ax.set_xticklabels(labels)
        ax.legend((centroid_bar[0], neighbors_bar[0], network_bar[0]),
                  ('Najbliższa średnia', 'K-najbliższych sąsiadów', 'Sieć neuronowa'))

        self.maximalize_window(plt)

        if save:
            extent = fig.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
            img_path = os.path.join(self.ROOT, self.IMG_DIR_NAME, 'best_results_features.png')
            fig.savefig(img_path, bbox_inches=extent.expanded(1.2, 1.2))

        if show:
            self.maximalize_window(plt)
            plt.show()


class NearestCentroid(Ploter):
    RESULTS_PATH = resource_filename('master_project', "results/results_NearestCentroid.json")
    IMG_DIR_NAME = 'nearest_centroid'

    def __init__(self):
        super().__init__()
        self.results = sorted(self.results, key=lambda res: res[0]['id'])

    def plot(self, show=True, save=False):
        import matplotlib.pyplot as plt

        subplot = [5, 2, 1]
        fig = plt.figure()
        title = 'Skuteczność algorytmu najbliższa średnia dla poszczegolnych wezlow drzewa'
        fig.canvas.set_window_title(title)

        for node in self.results:
            s_plot = fig.add_subplot(*subplot)
            features = [res['features'] for res in node]
            s_plot.plot(features, [res['ratio'] for res in node], 'ro')
            plt.xticks(features)
            subplot[2] += 1
            self.set_size(fig, 15, 20)
            self.set_labels(plt)

            self.maximalize_window(plt)
            if save:
                extent = s_plot.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
                name = self.normalize_name(node[0]['name'])
                img_path = os.path.join(self.ROOT, self.IMG_DIR_NAME, '{}.png'.format(name))
                fig.savefig(img_path, bbox_inches=extent.expanded(1.2, 1.4))

        if show:
            self.maximalize_window(plt)
            plt.show()


class NearestNeighbors(Ploter):
    RESULTS_PATH = resource_filename('master_project', "results/results_NearestNeighbors.json")
    BEST_PATH = resource_filename('master_project', "results/best_results.json")
    IMG_DIR_NAME = 'nearest_neighbors'

    def __init__(self):
        super().__init__()

    def get_bests(self):
        with open(self.BEST_PATH) as f:
            return [r[1] for r in json.load(f)]

    def plot(self, show=True, save=False):
        import matplotlib.pyplot as plt

        subplot = [5, 2, 1]
        fig = plt.figure()
        title = 'Skuteczność algorytmu k-najblizszych sasiadów dla poszczegolnych wezlow drzewa'
        fig.canvas.set_window_title(title)
        first, third = 0, 7
        bests = self.get_bests()

        for node in self.results:
            best_n, = [b['neighbors'] for b in bests if b['name'] == node[0][0]['name']]
            s_plot = fig.add_subplot(*subplot)
            features = [res[first]['features'] for res in node]
            _first = s_plot.plot(features, [res[first]['ratio'] for res in node])
            _second = s_plot.plot(features, [res[best_n - 2]['ratio'] for res in node])
            _third = s_plot.plot(features, [res[third]['ratio'] for res in node])
            plt.xticks(features)
            subplot[2] += 1
            self.set_size(fig, 15, 20)
            self.set_labels(plt)
            self.set_legend(plt, (_first[0], _second[0], _third[0]),
                            (node[0][first]['neighbors'], best_n, node[0][third]['neighbors']))

            self.maximalize_window(plt)
            if save:
                extent = s_plot.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
                name = self.normalize_name(node[0][first]['name'])
                img_path = os.path.join(self.ROOT, self.IMG_DIR_NAME, '{}.png'.format(name))
                fig.savefig(img_path, bbox_inches=extent.expanded(1.2, 1.4))

        if show:
            self.maximalize_window(plt)
            plt.show()


class NeuralNetwork(Ploter):
    RESULTS_PATH = resource_filename('master_project', "results/results_NeuralNetwork.json")
    IMG_DIR_NAME = 'neural_network'

    def __init__(self):
        super().__init__()

    def plot_one_layer(self, show=True, save=False):
        import matplotlib.pyplot as plt

        subplot = [5, 2, 1]
        fig = plt.figure()
        title = 'Skuteczność sieci neuronowej dla poszczegolnych wezlow drzewa'
        fig.canvas.set_window_title(title)
        first, second, third, fourfth = 0, 1, 2, 3

        for node in self.results:
            features = [res[first]['features'] for res in node]
            s_plot = fig.add_subplot(*subplot)
            _first = s_plot.plot(features, [res[first]['ratio'] for res in node])
            _second = s_plot.plot(features, [res[second]['ratio'] for res in node])
            _third = s_plot.plot(features, [res[third]['ratio'] for res in node])
            _fourfth = s_plot.plot(features, [res[fourfth]['ratio'] for res in node])
            plt.xticks(features)
            subplot[2] += 1
            self.set_size(fig, 15, 20)
            self.set_labels(plt)
            self.set_legend(plt, (_first[0], _second[0], _third[0], _fourfth[0]),
                            (node[0][num]['neurons'] for num in [first, second, third, fourfth]))

            self.maximalize_window(plt)
            if save:
                extent = s_plot.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
                name = self.normalize_name(node[0][first]['name'])
                img_path = os.path.join(self.ROOT, self.IMG_DIR_NAME, '1', '{}.png'.format(name))
                fig.savefig(img_path, bbox_inches=extent.expanded(1.2, 1.4))

        if show:
            self.maximalize_window(plt)
            plt.show()

    def plot_two_layer(self, show=True, save=False):
        import matplotlib.pyplot as plt

        subplot = [5, 2, 1]
        fig = plt.figure()
        title = 'Skuteczność sieci neuronowej dla poszczegolnych wezlow drzewa'
        fig.canvas.set_window_title(title)
        first, second, third, fourfth = 4, 5, 6, 7

        for node in self.results:
            s_plot = fig.add_subplot(*subplot)
            features = [res[first]['features'] for res in node]
            _first = s_plot.plot(features, [res[first]['ratio'] for res in node])
            _fourfth = s_plot.plot(features, [res[fourfth]['ratio'] for res in node])
            _second = s_plot.plot(features, [res[second]['ratio'] for res in node])
            _third = s_plot.plot(features, [res[third]['ratio'] for res in node])
            plt.xticks(features)

            subplot[2] += 1
            self.set_size(fig, 15, 20)
            self.set_labels(plt)
            self.set_legend(plt, (_first[0], _second[0], _third[0], _fourfth[0]),
                            (node[0][num]['neurons'] for num in [first, second, third, fourfth]))

            self.maximalize_window(plt)
            if save:
                extent = s_plot.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
                name = self.normalize_name(node[0][first]['name'])
                img_path = os.path.join(self.ROOT, self.IMG_DIR_NAME, '2', '{}.png'.format(name))
                fig.savefig(img_path, bbox_inches=extent.expanded(1.2, 1.4))

        if show:
            self.maximalize_window(plt)
            plt.show()

    def plot_three_layer(self, show=True, save=False):
        import matplotlib.pyplot as plt

        subplot = [5, 2, 1]
        fig = plt.figure()
        title = 'Skuteczność sieci neuronowej dla poszczegolnych wezlow drzewa'
        fig.canvas.set_window_title(title)
        first, second, third = 8, 9, 10

        for node in self.results:
            s_plot = fig.add_subplot(*subplot)
            features = [res[first]['features'] for res in node]
            _first = s_plot.plot(features, [res[first]['ratio'] for res in node])
            _second = s_plot.plot(features, [res[second]['ratio'] for res in node])
            _third = s_plot.plot(features, [res[third]['ratio'] for res in node])
            plt.xticks(features)
            subplot[2] += 1
            self.set_size(fig, 15, 20)
            self.set_labels(plt)
            self.set_legend(plt, (_first[0], _second[0], _third[0]),
                            (node[0][num]['neurons'] for num in [first, second, third]))

            self.maximalize_window(plt)
            if save:
                extent = s_plot.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
                name = self.normalize_name(node[0][first]['name'])
                img_path = os.path.join(self.ROOT, self.IMG_DIR_NAME, '3', '{}.png'.format(name))
                fig.savefig(img_path, bbox_inches=extent.expanded(1.2, 1.4))

        if show:
            self.maximalize_window(plt)
            plt.show()


class PlotMatrixes(Ploter):
    RESULTS_PATH = resource_filename('master_project', 'results/results_best_matrixes.json')
    IMG_DIR_NAME = 'matrixes'
    PRINT_MAP = {'NE': 'Sieć neuronowa',
                 'NN': 'K-najbliższych sąsiadów',
                 'NM': 'Najbliższa średnia'}

    def plot(self, show=True, save=False):
        import matplotlib.pylab as plt
        fig = plt.figure()

        subplot = [1, 3, 1] if save else [5, 6, 1]

        for node in sorted(self.results, key=lambda e: e['id']):
            if save:
                fig = plt.figure()
                subplot[2] = 1

            for name, values in node['values'].items():
                s_plot = fig.add_subplot(*subplot)
                nx = ny = len(values) + 1
                self.set_size(fig, nx / 2, ny + 1)
                data = [[num] + val for num, val in enumerate(values, 1)]
                data.insert(0, [' '] + [num for num, _ in enumerate(values, 1)])
                tb = s_plot.table(cellText=data, loc=(0, 0), cellLoc='center')
                tc = tb.properties()['child_artists']
                for cell in tc:
                    cell.set_height(1 / ny)
                    cell.set_width(1 / nx)

                ax = plt.gca()
                ax.set_ylabel(self.PRINT_MAP[name], fontsize=7)
                ax.set_xticks([])
                ax.set_yticks([])
                subplot[2] += 1

            if save:
                name = self.normalize_name(node['name'])
                img_path = os.path.join(self.ROOT, self.IMG_DIR_NAME, '{}.png'.format(name))
                fig.savefig(img_path)

        if show:
            self.maximalize_window(plt)
            plt.show()
        pp(self.results)


class FinalResults(Ploter):
    RESULTS_WITH_LEAF_PATH = resource_filename('master_project',
                                               'results/final_results_with_leaf.json')
    RESULTS_WITHOUT_LEAF_PATH = resource_filename('master_project',
                                                  'results/final_results_without_leaf.json')

    def __init__(self):
        pass

    def plot(self, show=True, save=False, with_leaf=True):
        title = 'Końcowy rezultat sytemu {} końcowych węzłów'
        img_path = 'best/final_results_{}_leaf.png'
        if with_leaf:
            title = title.format('z klasyfikacją')
            results = self.read_results(self.RESULTS_WITH_LEAF_PATH)
            img_path = img_path.format('with')
        else:
            title = title.format('bez klasyfikacji')
            results = self.read_results(self.RESULTS_WITHOUT_LEAF_PATH)
            img_path = img_path.format('without')

        import matplotlib.pylab as plt
        fig, ax = plt.subplots()
        ind = np.arange(1)  # the x locations for the groups
        width = 0.25       # the width of the bars
        ax.bar(ind, results['passed'], width, color='g')

        # add some text for labels, title and axes ticks
        ax.set_ylabel('Procent trafień')
        ax.set_title(title)
        ax.set_yticks(np.arange(0, 110, 10))
        plt.tick_params(axis='x', which='both', bottom='off', top='off', labelbottom='off')
        self.maximalize_window(plt)

        if save:
            extent = fig.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
            img_path = os.path.join(self.ROOT, img_path)
            fig.savefig(img_path, bbox_inches=extent.expanded(1.2, 1.2))

        if show:
            self.maximalize_window(plt)
            plt.show()

    def read_results(self, path):
        with open(path, 'r') as f:
            results = json.load(f)
        return results


if __name__ == '__main__':
    # BestResults().plot_times(False)
    # BestResults().plot_number_of_features(True)
    # NearestCentroid().plot(False)
    # NearestNeighbors().plot(True, True)
    # NeuralNetwork().plot_one_layer(False)
    # NeuralNetwork().plot_two_layer(False)
    # NeuralNetwork().plot_three_layer(False)
    # PlotMatrixes().plot(False, True)
    # FinalResults().plot(False, True, True)
    # FinalResults().plot(False, True, False)
    pass
