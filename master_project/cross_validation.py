# -*- coding: utf-8 -*-
"""
:author: Jaroslaw Grycz
:contact: jaroslaw.grycz@gmail.com
"""
from copy import deepcopy


def make_test(classifier, training, testing):
    classifier.train(training[:, 1:], training[:, 0])
    return classifier.test(testing[:, 1:], testing[:, 0])


def make_test_with_matrix(classifier, training, testing):
    classifier.train(training[:, 1:], training[:, 0])
    return classifier.get_test_matrix(testing[:, 1:], testing[:, 0])


def cross_validation(classifier, times=5):
    _pass = []
    _fail = []

    for _ in range(times):
        training, testing = classifier.prepare_datasets()
        p1, f1 = make_test(deepcopy(classifier), training, testing)
        p2, f2 = make_test(deepcopy(classifier), testing, training)
        _pass.append((p1 + p2) / 2.)
        _fail.append((f1 + f2) / 2.)

    pass_sum = sum(_pass) / 5
    fail_sum = sum(_fail) / 5
    return pass_sum / (pass_sum + fail_sum) * 100


def cross_validation_with_matrix(classifier):
    training, testing = classifier.prepare_datasets()
    res1 = make_test_with_matrix(deepcopy(classifier), training, testing)
    res2 = make_test_with_matrix(deepcopy(classifier), testing, training)
    res = res1 + res2
    keys = sorted(set([int(r[0]) for r in res] + [int(r[1]) for r in res]))
    results = [[0 for _ in keys] for __ in keys]
    for r, t in res:
        results[int(t) - 1][int(r) - 1] += 1

    return results
