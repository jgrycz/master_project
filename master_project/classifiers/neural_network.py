# -*- coding: utf-8 -*-
"""
:author: Jaroslaw Grycz
:contact: jaroslaw.grycz@gmail.com
"""
from sklearn.neural_network import MLPClassifier
from .base import Classifier


class NeuralNetwork(Classifier):
    def __init__(self, dataset, features=None, **kwargs):
        self.dataset = dataset
        self.features = features
        self.classifier = MLPClassifier(**kwargs)
