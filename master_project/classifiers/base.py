# -*- coding: utf-8 -*-
"""
:author: Jaroslaw Grycz
:contact: jaroslaw.grycz@gmail.com
"""
from abc import ABCMeta
import numpy as np


class Classifier(metaclass=ABCMeta):
    def prepare_datasets(self, number=2):
        np.random.shuffle(self.dataset)
        half = int(self.dataset.shape[0] / number)
        return self.dataset[:half, :], self.dataset[half:, :]

    def train(self, features, targets):
        self.classifier.fit(features, targets)

    def test(self, features, targets):
        results = self.classifier.predict(features)
        ratio = [r == t for r, t in zip(results, targets)]
        return ratio.count(True), ratio.count(False)

    def predict(self, features):
        return self.classifier.predict(features)

    def get_test_matrix(self, features, targets):
        results = self.classifier.predict(features)
        return list(zip(results, targets))
