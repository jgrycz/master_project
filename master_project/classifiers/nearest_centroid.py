# -*- coding: utf-8 -*-
"""
:author: Jaroslaw Grycz
:contact: jaroslaw.grycz@gmail.com
"""
from sklearn.neighbors import NearestCentroid as NCentroid
from .base import Classifier


class NearestCentroid(Classifier):
    def __init__(self, dataset, features=None):
        self.dataset = dataset
        self.features = features
        self.classifier = NCentroid()
