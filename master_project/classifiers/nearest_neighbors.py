# -*- coding: utf-8 -*-
"""
:author: Jaroslaw Grycz
:contact: jaroslaw.grycz@gmail.com
"""
from sklearn.neighbors import KNeighborsClassifier
from .base import Classifier


class NearestNeighbors(Classifier):
    def __init__(self, dataset, n_neighbors, features=None):
        self.dataset = dataset
        self.features = features
        self.classifier = KNeighborsClassifier(n_neighbors=n_neighbors,
                                               algorithm='brute',
                                               weights='distance')
