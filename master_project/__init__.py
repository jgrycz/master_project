# -*- coding: utf-8 -*-
"""
:author: Jaroslaw Grycz
:contact: jaroslaw.grycz@gmail.com
"""
from master_project import read_data
from master_project import pearson_corelation
from master_project import classes
