# -*- coding: utf-8 -*-
"""
:author: Jaroslaw Grycz
:contact: jaroslaw.grycz@gmail.com
"""
from master_project.pearson_corelation import Pearsonr
from master_project.read_data import get_dataset
from master_project.classes import anemia

from math import ceil
import os


def calculate_pearson_corelation_for_all_nodes():
    import matplotlib.pyplot as plt
    dataset = get_dataset()
    anemia.print_as_tree()
    subtypes = list(anemia.iter_on_all_nodes())
    length = len(subtypes)
    subplot = [ceil(length / 2), 2, 1]
    fig = plt.figure()
    fig.canvas.set_window_title('Korelacja pearsona dla poszczegolnych wezlow drzewa')

    for num, subtype in enumerate(subtypes, 1):
        target_dataset = subtype.prepare_data(dataset)
        pearsonr = Pearsonr(str(subtype))
        corelation_container = pearsonr.calculate_for_all_columns(target_dataset)
        corelation_container.print_data()
        subplot[2] = num
        corelation_container.plot(True if num == length else False, tuple(subplot), False)


def calculate_pearson_corelation_for_all_nodes_and_prepare_images():
    import matplotlib.pyplot as plt
    images = os.path.join(os.path.split(__file__)[0], "../docs/images/")
    dataset = get_dataset()
    anemia.print_as_tree()

    for subtype in anemia.iter_on_all_nodes():
        fig = plt.figure(figsize=(12, 4.5))
        print("Creating img for: {}".format(subtype.type))
        target_dataset = subtype.prepare_data(dataset)
        pearsonr = Pearsonr(str(subtype))
        corelation_container = pearsonr.calculate_for_all_columns(target_dataset)
        corelation_container.plot(False, None, False)
        name = subtype.type.replace(' ', '_').replace('.', '').replace('ć', 'c').replace('ś', 's')
        fig.savefig("{}/{}.png".format(images, name))


if __name__ == '__main__':
    # calculate_pearson_corelation_for_all_nodes_and_prepare_images()
    calculate_pearson_corelation_for_all_nodes()
