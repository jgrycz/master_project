# -*- coding: utf-8 -*-
"""
:author: Jaroslaw Grycz
:contact: jaroslaw.grycz@gmail.com
"""
from master_project.cross_validation import cross_validation, cross_validation_with_matrix
from master_project.classifiers import NearestNeighbors, NearestCentroid, NeuralNetwork
from master_project.read_data import get_normalized_dataset
from master_project.pearson_corelation import Pearsonr
from master_project.classes import anemia
from pkg_resources import resource_filename
import json


BEST_RESULTS = resource_filename('master_project', "results/best_results.json")


def frange(x, y, jump):
    while x < y:
        yield x
        x += jump


class Result:
    def __init__(self, type, name, ratio, features, neighbors=None, neurons=None):
        self.type = type
        self.name = name
        self.id = int(name.split('). ')[1].split('.')[0]) if name != '' else name
        self.ratio = ratio
        self.features = features
        self.neighbors = neighbors
        self.neurons = neurons

    def __str__(self):
        string = '{}\n{}\nRatio: {:2}\nIlsoc cech: {}\n'.format(self.type, self.name,
                                                                self.ratio, self.features)
        if self.neighbors is not None:
            string += 'Liczba sąsiadów: {}\n'.format(self.neighbors)
        if self.neurons is not None:
            string += 'Liczba sąsiadów: {}\n'.format(self.neurons)
        return string

    # __repr__ = __str__

    def get(self):
        d = {'type': self.type, 'name': self.name, 'ratio': self.ratio, 'id': self.id,
             'features': self.features, 'neighbors': self.neighbors, 'neurons': self.neurons}
        return {k: v for k, v in d.items() if v is not None}


class Clasify:
    def run(self):
        dataset = get_normalized_dataset()
        bests = []
        resutls = []
        for subtype in anemia.iter_on_all_nodes():
            best = Result('', '', 0, 0).get()
            sub_res = []
            for features in range(2, 33):
                res = self.make_test(subtype, dataset, features)
                sub_res.append(res)
                if type(res) is list:
                    potential_best = max(res, key=lambda r: r['ratio'])
                    if potential_best['ratio'] > best['ratio']:
                        best = potential_best
                elif res['ratio'] > best['ratio']:
                    best = res
            resutls.append(sub_res)
            print(str(best))
            bests.append(best)

        self.save_all_results(resutls)
        return sorted(bests, key=lambda r: r['id'])

    def _get_sorted_dataset(self, subtype, dataset):
        pearsonr = Pearsonr(str(subtype))
        target_dataset = subtype.prepare_data(dataset)
        sorted_dataset = pearsonr.make_permutation(target_dataset)
        return sorted_dataset

    def save_all_results(self, results):
        file_path = resource_filename('master_project', "results/results_{}.json".format(self.NAME))
        with open(file_path, 'w') as f:
            json.dump(results, f)


class ClasifyUsingNearestCentroid(Clasify):
    NAME = 'NearestCentroid'

    def make_test(self, subtype, dataset, features):
        sorted_dataset = self._get_sorted_dataset(subtype, dataset)
        classifier = NearestCentroid(sorted_dataset[:, :features])
        ratio = cross_validation(classifier)
        return Result(self.NAME, str(subtype), ratio, features).get()


class ClassifyUsingNearestNeighbors(Clasify):
    NAME = 'NearestNeighbors'

    def make_test(self, subtype, dataset, features):
        results = []
        for neighbors in range(2, 10):
            sorted_dataset = self._get_sorted_dataset(subtype, dataset)
            classifier = NearestNeighbors(sorted_dataset[:, :features], neighbors)
            ratio = cross_validation(classifier)
            results.append(Result(self.NAME, str(subtype), ratio, features, neighbors).get())

        return results


class ClassifyUsingNeuralNetwork(Clasify):
    NAME = 'NeuralNetwork'

    def make_test(self, subtype, dataset, features):
        results = []
        for neurons in ((30,), (50,), (100,), (200,), (30, 30), (50, 30), (100, 50), (200, 100)):
            sorted_dataset = self._get_sorted_dataset(subtype, dataset)
            classifier = NeuralNetwork(sorted_dataset[:, :features], hidden_layer_sizes=neurons)
            ratio = cross_validation(classifier)
            results.append(Result(self.NAME, str(subtype), ratio, features, neurons=neurons).get())
        return results


class ClasifyUsingBestResults:
    RESULTS_PATH = resource_filename('master_project', "results/best_results.json")

    def __init__(self):
        self.results = self.read_results()

    def read_results(self):
        with open(self.RESULTS_PATH, 'r') as f:
            results = json.load(f)
        return results

    def run(self):

        matrixes = []
        dataset = get_normalized_dataset()
        for subtype in anemia.iter_on_all_nodes():
            print('Working on: ' + str(subtype))
            res, = [res for res in self.results if res[0]['name'] == str(subtype)]
            sorted_dataset = self._get_sorted_dataset(subtype, dataset)
            centroid = NearestCentroid(sorted_dataset[:, :res[0]['features']])
            neighbors = NearestNeighbors(sorted_dataset[:, :res[1]['features']],
                                         res[1]['neighbors'])
            network = NeuralNetwork(sorted_dataset[:, :res[2]['features']],
                                    hidden_layer_sizes=res[2]['neurons'])

            mce = cross_validation_with_matrix(centroid)
            mni = cross_validation_with_matrix(neighbors)
            mne = cross_validation_with_matrix(network)
            matrixes.append({'id': res[1]['id'],
                             'name': res[1]['name'],
                             'values': {'NM': mce,
                                        'K-N': mni,
                                        'NN': mne}})

        self.save_all_results(matrixes)

    def _get_sorted_dataset(self, subtype, dataset):
        pearsonr = Pearsonr(str(subtype))
        target_dataset = subtype.prepare_data(dataset)
        sorted_dataset = pearsonr.make_permutation(target_dataset)
        return sorted_dataset

    def save_all_results(self, results):
        file_path = resource_filename('master_project', 'results/results_best_matrixes.json')
        with open(file_path, 'w') as f:
            json.dump(results, f)


class FinalClassification:
    RESULTS_PATH = resource_filename('master_project', "results/best_results.json")

    def __init__(self):
        self.results = self.read_results()

    def read_results(self):
        with open(self.RESULTS_PATH, 'r') as f:
            results = json.load(f)
        return results

    def link_best_classifiers(self):
        dataset = get_normalized_dataset()
        for subtype in anemia.iter_on_all_nodes():
            res, = [res for res in self.results if res[0]['name'] == str(subtype)]
            best = res[0] if res[0]['ratio'] >= res[1]['ratio'] else res[1]
            best = res[2] if res[2]['ratio'] > best['ratio'] else res[0]

            sorted_dataset = self._get_sorted_dataset(subtype, dataset)

            if best['type'] == 'NeuralNetwork':
                subtype.classifier = NeuralNetwork(sorted_dataset[:, :best['features']],
                                                   best['features'],
                                                   hidden_layer_sizes=best['neurons'])
            elif best['type'] == 'NearestNeighbors':
                subtype.classifier = NearestNeighbors(sorted_dataset[:, :best['features']],
                                                      best['neighbors'],
                                                      best['features'])
            else:
                subtype.classifier = NearestCentroid(sorted_dataset[:, :best['features']],
                                                     best['features'])
            _, half = subtype.classifier.prepare_datasets(2)
            subtype.classifier.train(half[:, 1:], half[:, 0])

    def _run(*args, **kwargs):
        raise NotImplementedError

    def run(self):
        self.link_best_classifiers()
        results = {'passed': 0, 'failed': 0}
        dataset = get_normalized_dataset()
        for _ in range(5):
            for row in dataset:
                current_node = anemia
                self._run(current_node, dataset, row, results)

        print(results)
        all_res = len(dataset) * 5
        results['passed'] /= (all_res / 100)
        results['failed'] /= (all_res / 100)
        self.save_all_results(results, self.FINAL_RESULTS_PATH)

    def _get_sorted_dataset(self, subtype, dataset, row=None):
        pearsonr = Pearsonr(str(subtype))
        target_dataset = subtype.prepare_data(dataset)
        sorted_dataset = pearsonr.make_permutation(target_dataset, row=row)
        return sorted_dataset

    def save_all_results(self, results, file_name):
        file_path = resource_filename('master_project', 'results/{}'.format(file_name))
        with open(file_path, 'w') as f:
            json.dump(results, f)


class FinalClassificationWithLeaf(FinalClassification):
    FINAL_RESULTS_PATH = 'final_results_with_leaf.json'

    def _run(self, current_node, dataset, row, results):
        sorted_row = self._get_sorted_dataset(current_node, dataset, row)
        res = int(current_node.classifier.predict(
            sorted_row[1:current_node.classifier.features])[0])
        start, _ = current_node.range.get()
        cls = res + start - 1
        if not current_node.subtypes[res - 1].is_node() and cls == int(row[0]):
            results['passed'] += 1
            return
        elif not current_node.subtypes[res - 1].is_node():
            results['failed'] += 1
            return
        else:
            current_node = current_node.subtypes[res - 1]
            return self._run(current_node, dataset, row, results)


class FinalClassificationWithoutLeaf(FinalClassification):
    FINAL_RESULTS_PATH = 'final_results_without_leaf.json'

    def _run(self, current_node, dataset, row, results):
        sorted_row = self._get_sorted_dataset(current_node, dataset, row)
        res = int(current_node.classifier.predict(
            sorted_row[1:current_node.classifier.features])[0])
        start, stop = current_node.range.get()
        cls = int(row[0])
        if not current_node.subtypes[res - 1].subtypes[0].is_node() and start <= cls <= stop:
            results['passed'] += 1
            return
        elif not current_node.subtypes[res - 1].subtypes[0].is_node():
            results['failed'] += 1
            return
        else:
            current_node = current_node.subtypes[res - 1]
            return self._run(current_node, dataset, row, results)


def main():
    ne = ClassifyUsingNeuralNetwork().run()
    nc = ClasifyUsingNearestCentroid().run()
    nn = ClassifyUsingNearestNeighbors().run()
    summary = [(c, n, e) for c, n, e in zip(nc, nn, ne)]
    with open(BEST_RESULTS, 'w') as f:
        json.dump(summary, f)


def main2():
    ClasifyUsingBestResults().run()


if __name__ == '__main__':
    FinalClassificationWithLeaf().run()
    FinalClassificationWithoutLeaf().run()
