# -*- coding: utf-8 -*-
"""
:author: Jaroslaw Grycz
:contact: jaroslaw.grycz@gmail.com
"""
import logging


def get_logger():
    return logging.getLogger("master_project")
