# README #

The aim of repository is store final master project for study.

**_Multistage computer-aided of anemiia in children - algorithms and experimental investigations._**

_Wieloetapowa komputerowa diagnostyka anemii u dzieci - algorytmy i badania eksperymentalne._

This README would normally document whatever steps are necessary to get your application up and running.

Owner: **Jaroslaw Grycz**  
Contact: **jaroslaw.grycz@gmail.com**

Requirements:  
- [Python 3.5](https://www.python.org/downloads/release/python-350/)  
- [scipy](https://www.scipy.org/)  
- [numpy](http://www.numpy.org/)  
- [scikit-learn](http://scikit-learn.org/)  
- [matplotlib](https://matplotlib.org/)  
- [treelib](https://github.com/caesar0301/treelib)  
- [pytest](https://docs.pytest.org/en/latest/)  
- [flake8](http://flake8.pycqa.org/en/latest/)  
- [pytest-flake8](https://pypi.python.org/pypi/pytest-flake8)  
- [check-manifest](https://pypi.python.org/pypi/check-manifest)  
- [coverage](https://coverage.readthedocs.io/en/coverage-4.3.4/)  
- [tox](https://tox.readthedocs.io/en/latest/)  


For install:
```bash
git clone https://jgrycz@bitbucket.org/jgrycz/master_project.git
cd master_project
pip install -e . -r requirements.cfg
```

Create master_project-*.tar.gz
```bash
python setup.py sdist
```

For run tests:
```bash
python3 -m pytest
```

Create ma thesis, with update biblography:
```bash
pdflatex ma_thesis.tex
bibtex ma_thesis.aux
pdflatex ma_thesis.tex
pdflatex ma_thesis.tex
```

Check spell
```bash
aspell check ma_thesis.tex
```
